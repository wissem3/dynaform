# Dynaform

Dynaform a cool name that we've chosen for this frontend challenge.


We've seen a lot of SAAS tools and fancy web applications and almost all share what is called CRUD screens (create/read/update/delete). CRUD screens usually contain some sorte of input fields with validations and a set of buttons : save update delete, this is commonly called forms.
The actions behind the form buttons are usually inpacting a an entity in the backend (which is usually a table in a relational database).


We though it would be cool to come up with a dynamic view that creates a form based on the schema of a table.


Here is a non exhaustif mapping of schema field types and UI input types.


Schema Field type   | UI Input type
-------------  | -------------
String  | Free input with a string value validator
Numeric   | Free input with a numeric value validator
Enum | Combobox input with a value within items validator
Date | Date input with date value validator

## Specification

Dynaform is most likely to be used as a library but we would love to see it in action. For the sake of the challenge let's assume that Dynaform is a web application with a single view (route). This main view takes as a property `the schema` of a table and renders the dynamic form.

Let's assume that this web application receives the schema via a REST api call, you will be asked to create a mocked up backend call to `/schema`. This mocked up call will return a table schema that your main view will use to dynamically create the form accordingly.

An example of schema returned by the mocked call:

```
{
    "name":"User",
    "fields":[
        {
            "name":"first_name",
            "type": "String"
        },
        {
            "name":"last_name",
            "type": "String"
        },
        {
            "name":"address",
            "type": "String"
        },
        {
            "name":"gender",
            "type": "GenderEnum('M','F')"
        },
        {
            "name":"birth_date",
            "type": "Date"
        },

    ]
}
```

## Requirements
Use the following to implement Dynaform:
- Vue.js and Typescript as your weapon of choice
- End-to-end tests to make sure Dynaform is solid
- Leverage the tests you wrote by creating a gitlab pipeline that will lint, test and build Dynaform package.
- Share your solution as a merge request to this repository.


## Hint !

Feel free to create a personal gitlab/github repository and share your achievement there. We developers love to have a fournished github page :) 


